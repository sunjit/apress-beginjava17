package com.jdojo.cls;

/*
 * 1. What is the output of last line: 
 * System.out.println("Count " + Human.count);
 * 
 * 2. 
 */
public class FieldAccessTest {
	public static void main(String[] args) {
		Human jack = new Human();
		Human.count++;

		jack.name = "Jack Parker";
		jack.gender = "Male";

		// Read values
		String jackName = jack.name;
		String jackGender = jack.gender;
		long population = ++jack.count;

		System.out.println(jackName + " : " + jackGender + " : " + population);
		
		jack.name = "Jackie Parker";
		String changedName = jack.name;
		System.out.println("Changed name " + changedName);
		System.out.println("Count " + Human.count);
	}
}
